# Laravel 8 + Vue + Vuex - Área Local

## Setup

### Dependências (php ^7.4)

-   composer update
-   gerar arquivo .env
-   php artisan key:generate
-   php artisan jwt:secret

### Webpack

-   yarn ou npm install

### build

-   yarn watch ou npm run watch (assistir modificações nos arquivos js e scss)
-   yarn dev ou npm run dev (compila arquivos js e css)
-   yarn production ou npm run production ( minifica os arquivos js e scss para produção )

#

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
