<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Planos;
use Illuminate\Http\Request;
use App\Models\Property;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class AnuncioController extends Controller
{
    //
    public function index(){
        // return Property::all();
        $properties = Property::where('users_id', Auth::user()->id)->get();
        foreach($properties as $property){
            $dt_created = $property->created_at;
            $dt_created = Carbon::parse($dt_created);
            $diff = $dt_created->diffInDays(Carbon::now());
            $property->days_after_create = $diff;

            $property->dt_criacao = $property->created_at->format('d/m/Y');

        }

        return $properties;
    }

    //
    public function getPlano(){
        $plano = Planos::where('id', '=', Auth::user()->plano)->first();
        $qtd_publicados_plano = Property::where('users_id', '=', Auth::user()->id)->count();
        return json_encode(array('plano' => $plano, 'qtd_publicados_plano' => $qtd_publicados_plano));
    }

    //
    public function changeStatus($property_id){
        $property = Property::find($property_id);
        $property->status = $property->status == 1 ? 0 : 1;

        if($property->save()){
            return response()->json('success', 200);
        }
        return response()->json('error', 422);
    }
}
