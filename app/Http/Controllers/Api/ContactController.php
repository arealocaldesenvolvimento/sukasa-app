<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;

class ContactController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        Mail::send('email', [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'subject' => $request->get('subject'),
            'messages' => $request->get('message') ],
            function ($message) {
                    $message->from('site@sukasa.com.br');
                    $message->to('site@sukasa.com.br', 'Sukasa')
                    ->subject('Contato do site');
        });

        return response()->json('Mail send', 200);
    }
}
