<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use App\Services\PropertyService;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Models\Property;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class ImportController extends Controller
{
    public function __construct(PropertyService $service)
    {
        $this->service = $service;
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $files = $request->file('file');
        $properties = json_decode(file_get_contents($files), true);
        $i = 0;

        foreach ($properties as $property) {
            $i++;
            $lat = '';
            $lng = '';

            // Remove os espaços em branco
            $property['address'] = trim($property['address']);
            $property['neighborhood'] = trim($property['neighborhood']);
            $property['description'] = trim($property['description']);
            $property['city'] = trim($property['city']);
            $property['state'] = trim($property['state']);
            $property['minimal_value'] = trim($property['minimal_value']);
            $property['avaliation_value'] = trim($property['avaliation_value']);
            $property['sale_module'] = trim($property['sale_module']);

            // Concatena os dados de endereço para a pesquisa do google ficar mais precisa
            $address = $property['address'] . ',' . $property['city'] . '-' . $property['state'];

            if ($response = $this->get_coordinates($address)) {
                if (!empty(json_decode($response)->results[0])) {
                    $lat = json_decode($response)->results[0]->geometry->location->lat;
                    $lng = json_decode($response)->results[0]->geometry->location->lng;
                }
            }
            $type = explode(",", $property['description'])[0];

            $payload = Arr::collapse(
                [
                    $property,
                    [
                        'type' => trim($type),
                        'latitude' => trim($lat),
                        'longitude' => trim($lng)
                    ]
                ]
            );
            $this->service->make($payload);
        }

        return response()->json(['imported rows' => $i], 200);
    }

    protected function get_coordinates(String $address)
    {
        return Http::get('https://maps.googleapis.com/maps/api/geocode/json', [
            'address' => $address,
            'key' => env('GOOGLE_API_KEY')
        ]);
    }

    /**
     * Importa imóveis de um arquivo JSON.
     * Remove todos os imóveis existentes e insere os novos do arquivo.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function importProperties(Request $request)
    {
        try {
            // Assume que o arquivo JSON é enviado via upload
            $file = $request->file('jsonFile');

            // Verifica se o arquivo foi carregado corretamente
            if (!$file || !$file->isValid()) {
                throw new Exception("Erro no upload do arquivo.");
            }

            $filePath = $file->getPathname();
            $jsonContent = File::get($filePath);
            $properties = json_decode($jsonContent, true);

            if (is_null($properties)) {
                throw new Exception("Erro ao decodificar JSON do arquivo.");
            }

            // Inicia uma transação
            DB::beginTransaction();

            // Remove todos os imóveis existentes
            Property::truncate();

            // Insere os novos imóveis
            foreach ($properties as $propertyData) {
                // [Código para tratar valores numéricos]

                // Obter detalhes de localização para o endereço
                $locationDetails = $this->get_location_details($propertyData['address']);

                $propertyData = array_merge($propertyData, $locationDetails);

                // Insere o imóvel no banco de dados
                Property::create($propertyData);
            }

            // Confirma a transação
            DB::commit();

            return response()->json(['message' => 'Imóveis importados com sucesso.'], 200);
        } catch (Exception $e) {
            // Em caso de erro, desfaz as alterações
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Retorna as coordenadas geográficas e informações adicionais de um endereço.
     *
     * @param String $address
     * @return array
     */
    protected function get_location_details(String $address)
    {
        // Primeira tentativa: usando o endereço completo
        $locationDetails = $this->attemptLocationFetch($address);

        // Valores padrão para latitude e longitude (Brasília)
        $defaultLatitude = '-15.7934036';
        $defaultLongitude = '-47.8823172';

        // Verifica se os valores padrão foram retornados, indicando falha na localização
        if ($locationDetails['latitude'] === $defaultLatitude || $locationDetails['longitude'] === $defaultLongitude) {
            // Tenta novamente usando apenas o CEP, se possível
            if (preg_match('/\b\d{5}-\d{3}\b/', $address, $matches)) {
                $zipCode = $matches[0];
                $locationDetails = $this->attemptLocationFetch($zipCode);
            }
        }

        return $locationDetails;
    }

    protected function attemptLocationFetch(String $query)
    {
        $response = Http::get('https://maps.googleapis.com/maps/api/geocode/json', [
            'address' => $query,
            'key' => env('GOOGLE_API_KEY')
        ]);

        // Valores padrão: ponto central do Brasil (Brasília)
        $defaultLatitude = '-15.7934036';
        $defaultLongitude = '-47.8823172';
        $defaultNeighborhood = 'Centro';
        $defaultCity = 'Brasília';
        $defaultState = 'Distrito Federal';

        if ($response->successful()) {
            $data = $response->json();

            if (isset($data['results'][0])) {
                $location = $data['results'][0]['geometry']['location'];
                $addressComponents = $data['results'][0]['address_components'];

                return [
                    'latitude' => $location['lat'] ?? $defaultLatitude,
                    'longitude' => $location['lng'] ?? $defaultLongitude,
                    'neighborhood' => $this->extractAddressComponent($addressComponents, 'sublocality') ?? $defaultNeighborhood,
                    'city' => $this->extractAddressComponent($addressComponents, 'locality') ?? $defaultCity,
                    'state' => $this->extractAddressComponent($addressComponents, 'administrative_area_level_1') ?? $defaultState,
                ];
            }
        }

        // Retorna os valores padrão se a API não encontrar o endereço
        return [
            'latitude' => $defaultLatitude,
            'longitude' => $defaultLongitude,
            'neighborhood' => $defaultNeighborhood,
            'city' => $defaultCity,
            'state' => $defaultState
        ];
    }


    protected function extractAddressComponent($components, $type)
    {
        foreach ($components as $component) {
            if (in_array($type, $component['types'])) {
                $name = $component['long_name'];

                if ($type === 'administrative_area_level_1') {
                    $name = str_replace('State of ', '', $name);
                }

                return $name;
            }
        }
        // Fallback para 'administrative_area_level_2' se 'locality' não for encontrado
        if ($type === 'locality') {
            foreach ($components as $component) {
                if (in_array('administrative_area_level_2', $component['types'])) {
                    return $component['long_name'];
                }
            }
        }

        return null;
    }
    public function importSingleProperty(Request $request)
    {
        try {
            $propertyData = $request->all();

            $locationDetails = $this->get_location_details($propertyData['address']);
            $propertyData = array_merge($propertyData, $locationDetails);

            Property::create($propertyData);

            return response()->json(['message' => 'Imóvel importado com sucesso.'], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function deleteAllProperties()
    {
        try {
            // Exclui todos os imóveis
            Property::truncate();

            return response()->json(['message' => 'Todos os imóveis foram excluídos com sucesso.'], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }


    public function testLocationFetch($query)
    {
        $response = Http::get('https://maps.googleapis.com/maps/api/geocode/json', [
            'address' => $query,
            'key' => env('GOOGLE_API_KEY')
        ]);

        $data = $response->json();

        return response()->json($data);
    }
}
