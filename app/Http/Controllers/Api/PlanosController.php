<?php

namespace App\Http\Controllers\Api;

use App\Models\Planos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StorePlanos;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use MercadoPago\SDK;
use MercadoPago\Preference;
use MercadoPago\Item;
use MercadoPago\Payer;

class PlanosController extends Controller
{
    public function __construct(Planos $plano)
    {
        $this->plano = $plano;
        $this->middleware('isAdmin')->except(['index', 'buyPlano', 'storePlano', 'checkPayment']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $planos = Planos::all();
        return response()->json([ 'planos' => $planos], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePlanos $request)
    {
        //
        $plano = $this->plano->fill($request->all());
        if($plano->save()){
            return response()->json('success', 200);
        }
        return response()->json('error', 422);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Planos  $planos
     * @return \Illuminate\Http\Response
     */
    public function show(Planos $planos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Planos  $planos
     * @return \Illuminate\Http\Response
     */
    public function edit(Planos $planos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Planos  $planos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $plano = Planos::find($id);
        $plano->fill($request->all());
        if($plano->save()){
            return response()->json('success', 200);
        }
        return response()->json('error', 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Planos  $planos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if ( $this->plano->destroy($id)) {
            return response()->json('ok', 200);
        }
        return response()->json('error', 422);
    }

    //
    function buyPlano($plano_id){
        $plano = Planos::find($plano_id);

        SDK::setAccessToken(env('MP_API_ACCESS_TOKEN'));
        $preference = new Preference();
        $item = new Item();
        $payer = new Payer();
        $user = User::find(Auth::user()->id);

        $user->plano = $plano->id;

        if($user->save()){
            $payer->name = Auth::user()->name;
            $payer->email = Auth::user()->email;
            $payer->identification = array(
                'type' => 'CPF',
                'number' => Auth::user()->cpfcnpj
            );

            $item->title = $plano->title;
            $item->quantity = 1;
            $item->unit_price = $plano->valor;
            $item->currency_id = "BRL";

            $preference->back_urls = array(
                'success' => url('anunciante'),
                'failure' => url('anunciante'),
                'pending' => url('anunciante')
            );
            $preference->payer = $payer;
            $preference->items = array($item);
            $preference->save();

            return response()->json(['preference_id' => $preference->id, 'public_key' => env('MP_API_PUBLIC_KEY')], 200);
        }
        return response()->json('error', 422);
    }

    //
    function storePlano(Request $request){
        $user = User::find(Auth::user()->id);

        $user->payment_id = $request->payment_id;
        $user->payment_status = $request->status;

        if($user->save()){
            return response()->json('ok', 200);
        }

        return response()->json('error', 422);
    }

    //
    function checkPayment(){
        $response = Http::withToken(env('MP_API_ACCESS_TOKEN'))->get('https://api.mercadopago.com/v1/payments/'.Auth::user()->payment_id);
        if($response->ok()){
            if($response->json()['status'] != Auth::user()->payment_status){
                User::where('id', Auth::user()->id)
                    ->update(['payment_status' => $response->json()['status']]);
            }
            return response()->json($response->json(), 200);
        }

    }
}
