<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Post_Category;
use File;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    public function __construct(){
        $this->middleware('isAdmin')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::all();
        return response()->json([ 'posts' => $post], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new Post;
        $post->title = $request->title;
        $post->description = $request->description;
        $post->category_id = $request->category_id;

        if ($post->save()) {
            if ( $request->file('thumbnail') && $request->file('thumbnail')->isValid() ) {

                $name = $post->id.'_'.$request->file('thumbnail')->getClientOriginalName();
                $path = $request->file('thumbnail')->storeAs('/images/noticias', $name, 'public');
                $post->thumbnail = '/storage\/'.$path;

                if ( $post->save() ) {
                    return response()->json('success', 200);
                }

            } else {
                return response()->json('success', 200);
            }
        }

        return response()->json('erro', 422);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        return response()->json(['post' => $post], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);

        if ( $request->file('thumbnail') && $request->file('thumbnail')->isValid() ) {
            $name = $post->id.'_'.$request->file('thumbnail')->getClientOriginalName();
            $path = $request->file('thumbnail')->storeAs('/images/noticias', $name, 'public');
            $post->thumbnail = '/storage\/'.$path;

        }

        if (!empty($post)) {
            $post->fill($request->except('thumbnail', '_method'))->save();
            return response()->json($post, 200);
        }

        return response()->json('error', 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $path = str_replace('/storage\/', '', $post->thumbnail);
        if(Storage::disk('public')->delete($path)){
            if (Post::destroy($id)) {
                return response()->json('ok', 200);
            }
        }
        return response()->json('error', 422);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCategories(){
        $categories = Post_Category::all();
        return response()->json([ 'categories' => $categories], 200);
    }
}
