<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\StoreProperty;
use App\Http\Controllers\Controller;
use App\Services\PropertyService;
use Illuminate\Http\Request;
use App\Models\Property;
use File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PropertyController extends Controller
{
    public function __construct(PropertyService $service, Property $property)
    {
        $this->property = $property;
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Property::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProperty $request)
    {
        $property = $this->property->fill($request->except('files', '_method'));
        $property->users_id = Auth::user()->id;

        $images = array();
        if ($property->save()) {

            if ($request->file('files')) {
                foreach ($request->file('files') as $key => $file) {
                    if ($file->isValid()) {
                        $name = $property->id . '_' . $file->getClientOriginalName();

                        $path = $file->storeAs('/images/imoveis', $name, 'public');
                        $images[] = $path;
                    }
                }

                $property->images = $images;

                if ($property->save()) {
                    return response()->json('success', 200);
                }
            } else {
                return response()->json('success', 200);
            }
        }

        return response()->json('error', 422);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $property = Property::find($id);
        $property->users_id = Auth::user()->id;

        $images = $property->images;
        if (!empty($property)) {
            $property->fill($request->except('file', '_method'));
            if ($property->save()) {

                if ($request->file('files')) {
                    foreach ($request->file('files') as $key => $file) {
                        if ($file->isValid()) {
                            $name = $property->id . '_' . $file->getClientOriginalName();

                            $path = $file->storeAs('/images/imoveis', $name, 'public');
                            $images[] = $path;
                        }
                    }

                    $property->images = $images;

                    if ($property->save()) {
                        return response()->json('success', 200);
                    }
                } else {
                    return response()->json('success', 200);
                }
            }
            return response()->json('success', 200);
        }

        return response()->json('error', 422);

        // $path = [];
        // if ( $request->file('file') && $request->file('file')->isValid() ) {
        //     if ( !file_exists(  public_path() . '/images/imoveis/' . $id . '/') ) {
        //         File::makeDirectory( public_path() . '/images/imoveis/' . $id, $mode = 0777, true, true);
        //     }
        //     $request->file('file')->move( public_path() . '/images/imoveis/' . $id, $request->file('file')->getClientOriginalName());
        //     $path = '/images/imoveis/' . $id . '/' . $request->file('file')->getClientOriginalName();
        //     $request->merge(['images' => $path]);
        // }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->property->destroy($id)) {
            return response()->json('ok', 200);
        }

        return response()->json('error', 422);
    }

    /**
     * Filter the specified resource from storage.
     *
     * @param  string  $filter
     * @return \Illuminate\Http\Response
     */
    public function filter($option)
    {
        $options = $this->service->filterByOption($option);
        $items = [];
        foreach ($options as $item) {
            array_push($items, $item[$option]);
        }

        return response()->json($items, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteImage(Request $request)
    {
        $images = $request->images;
        $path_delete = $request->path_delete;
        $deletado = false;

        foreach ($images as $key => $image) {
            if ($image == $path_delete) {
                unset($images[$key]);
                $deletado = true;
            }
        }

        if ($deletado) {
            $property = Property::find($request->id);
            $property->images = $images;
            if ($property->save()) {
                $this->removeFromStorage($path_delete);
                return response()->json($property, 200);
            }
        }
        return response()->json('error', 422);
    }

    /*
    *
    * Função que remove a imagem da pasta de storage
    */
    public function removeFromStorage($path)
    {
        return Storage::disk('public')->delete($path);
    }

    //
    // public function getPublished(){
    //     return Property::where('status', 1)->get();
    // }
    public function getPublished(Request $request)
    {
        $ne = $request->input('northEast');
        $sw = $request->input('southWest');

        // Verifica se as coordenadas foram recebidas corretamente
        if (!$ne || !$sw || !isset($ne['lat']) || !isset($ne['lng']) || !isset($sw['lat']) || !isset($sw['lng'])) {
            return response()->json(['error' => 'Coordenadas inválidas.'], 400);
        }

        // Converte para float para garantir o tipo correto
        $neLat = (float) $ne['lat'];
        $neLng = (float) $ne['lng'];
        $swLat = (float) $sw['lat'];
        $swLng = (float) $sw['lng'];

        // Concatena as variáveis diretamente na string SQL
        $sql = "SELECT * FROM `property`
                WHERE `status` = 1
                AND `latitude` <= $neLat
                AND `latitude` >= $swLat
                AND `longitude` >= $swLng
                AND `longitude` <= $neLng";

        $properties = DB::select(DB::raw($sql));

        return $properties;
    }

    public function getUniqueSaleModules() {
        $saleModules = Property::distinct()->pluck('sale_module');
        return response()->json($saleModules);
    }

    public function getUniquePropertyTypes() {
        $propertyTypes = Property::distinct()->pluck('type');
        return response()->json($propertyTypes);
    }
}
