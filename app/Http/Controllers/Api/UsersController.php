<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUser;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class UsersController extends Controller
{

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->middleware('isAdmin');
    }

    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return response()->json([ 'users' => $users], 200);
    }

    //
    public function store(StoreUser $request){

        $user = $this->user->fill($request->except('_method'));
        if($user->save()){
            return response()->json('success', 200);
        }
        return response()->json('error', 422);
    }

    //
    public function update(Request $request, $id){

        $user = User::find($id);
        $user->fill($request->all());
        if($user->save()){
            return response()->json('success', 200);
        }
        return response()->json('error', 422);
    }

    //
    public function destroy($id){

        if ( $this->user->destroy($id) && Auth::user()->id != $id) {
            return response()->json('ok', 200);
        }

        return response()->json('error', 422);
    }

    //
    function checkPayment($id){
        $user = User::find($id);

        $response = Http::withToken(env('MP_API_ACCESS_TOKEN'))->get('https://api.mercadopago.com/v1/payments/'.$user->payment_id);
        if($response->ok()){
            if($response->json()['status'] != $user->payment_status){
                User::where('id', $user->id)
                    ->update(['payment_status' => $response->json()['status']]);
            }
            return response()->json($response->json(), 200);
        }

    }
}
