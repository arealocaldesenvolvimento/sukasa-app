<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreProperty extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'avaliation_value' => 'required',
            'sale_module' => 'required',
            'type' => 'required'
        ];
    }

    /**
     * Get the validation rules messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'address.required' => 'O campo ENDEREÇO é obrigatório',
            'city.required' => 'O campo CIDADE é obrigatório',
            'state.required' => 'O campo ESTADO é obrigatório',
            'latitude.required' => 'O campo LATITUDE é obrigatório',
            'longitude.required' => 'O campo LONGITUDE é obrigatório',
            'avaliation_value.required' => 'O campo VALOR DE AVALIAÇÃO é obrigatório',
            'sale_module.required' => 'O campo MÓDULO DE VENDA é obrigatório',
            'type.required' => 'O campo TIPO é obrigatório',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
