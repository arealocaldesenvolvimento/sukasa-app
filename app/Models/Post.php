<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $table = 'post';

    protected $fillable = [
        'title',
        'description',
        'thumbnail',
        'category_id'
    ];

    protected $attributes = [
        'category_id' => 1
    ];
}
