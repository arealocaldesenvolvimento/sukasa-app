<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Casts\ParseFloat;

class Property extends Model
{
    use HasFactory;

    protected $table = 'property';

    protected $fillable = [
        'address',
        'neighborhood',
        'status',
        'city',
        'state',
        'latitude',
        'longitude',
        'minimal_value',
        'avaliation_value',
        'sale_module',
        'discount',
        'description',
        'type',
        'users_id',
        'payment_method',
        'images',
        'link',
        'identificador',
    ];

    protected $attributes = [
        'status' => 1,
        'users_id' => 1,
        'images' => null,
        'payment_method' => '{
            "todos" : true
        }'
    ];

    protected $casts = [
        'discount' => ParseFloat::class,
        'payment_method' => 'array',
        'images' => 'array'
    ];

    public function scopeFilters($query, $option)
    {
        return $query->select($option);
    }
}
