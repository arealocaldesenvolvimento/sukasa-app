<?php

namespace App\Services;

use App\Models\Property;

class PropertyService
{
  public function __construct(Property $property)
  {
    $this->property = $property;
  }

  /**
   * Make a new model
   *
   * @param  App\Models\Property  $model
   */
  public function make(array $data)
  {
    return $this->property->create($data);
  }

  /**
   * Filter model
   *
   * @param  App\Models\Property  $model
   */
  public function filterByOption(String $option)
  {
    return $this->property->filters($option)->groupby($option)->get();
  }
}
