(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[15],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/landingPage/Header.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/babel-loader/lib??ref--10-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/components/landingPage/Header.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var google_maps_api_loader__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! google-maps-api-loader */ "./node_modules/google-maps-api-loader/index.js");
/* harmony import */ var google_maps_api_loader__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(google_maps_api_loader__WEBPACK_IMPORTED_MODULE_1__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
  try {
    var info = gen[key](arg);
    var value = info.value;
  } catch (error) {
    reject(error);
    return;
  }

  if (info.done) {
    resolve(value);
  } else {
    Promise.resolve(value).then(_next, _throw);
  }
}

function _asyncToGenerator(fn) {
  return function () {
    var self = this,
        args = arguments;
    return new Promise(function (resolve, reject) {
      var gen = fn.apply(self, args);

      function _next(value) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
      }

      function _throw(err) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
      }

      _next(undefined);
    });
  };
} //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Header",
  data: function data() {
    return {
      disabled: true,
      links: [{
        name: "QUEM SOMOS",
        route: {
          name: "quem-somos"
        }
      }, {
        name: "BLOG",
        route: {
          name: "blog"
        }
      }, {
        name: "FALE CONOSCO",
        route: {
          name: "fale-conosco"
        }
      }, {
        icon: "mdi-instagram",
        route: {
          name: "fale-conosco"
        }
      }, {
        icon: "mdi-facebook",
        route: {
          name: "fale-conosco"
        }
      }]
    };
  },
  mounted: function mounted() {
    var _this = this;

    return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
      var googleMapApi, places, self;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return google_maps_api_loader__WEBPACK_IMPORTED_MODULE_1___default()({
                libraries: ["places"],
                apiKey: _this.apiKey
              });

            case 2:
              googleMapApi = _context2.sent;
              _this.google = googleMapApi;
              places = new google.maps.places.Autocomplete(_this.$refs.address);
              self = _this;
              google.maps.event.addListener(places, "place_changed", /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
                var place, componentMap, i, types, j, component_type;
                return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        _context.next = 2;
                        return places.getPlace();

                      case 2:
                        place = _context.sent;
                        componentMap = {
                          administrative_area_level_2: "administrative_area_level_2"
                        };
                        i = 0;

                      case 5:
                        if (!(i < place.address_components.length)) {
                          _context.next = 20;
                          break;
                        }

                        types = place.address_components[i].types;
                        j = 0;

                      case 8:
                        if (!(j < types.length)) {
                          _context.next = 17;
                          break;
                        }

                        component_type = types[j];

                        if (!componentMap.hasOwnProperty(component_type)) {
                          _context.next = 14;
                          break;
                        }

                        _context.next = 13;
                        return self.$store.commit("PropertyModule/MUTATE_MAPCENTER", place.geometry);

                      case 13:
                        _this.disabled = false;

                      case 14:
                        j++;
                        _context.next = 8;
                        break;

                      case 17:
                        i++;
                        _context.next = 5;
                        break;

                      case 20:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee);
              })));

            case 7:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }))();
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/landingPage/Header.vue?vue&type=template&id=40dd73b6&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/components/landingPage/Header.vue?vue&type=template&id=40dd73b6& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("header", [
    _c("div", { staticClass: "stripe" }, [
      _c("div", { staticClass: "al-container al-mobile-container" }, [
        _c(
          "ul",
          { staticClass: "navbar-options" },
          _vm._l(_vm.links, function(link, i) {
            return _c(
              "li",
              { key: i },
              [
                link.name
                  ? _c("router-link", { attrs: { to: link.route } }, [
                      _vm._v(
                        "\n                        " +
                          _vm._s(link.name) +
                          "\n                    "
                      )
                    ])
                  : _c(
                      "router-link",
                      { attrs: { to: link.route } },
                      [
                        _c("v-icon", { attrs: { color: "#fff" } }, [
                          _vm._v(
                            "\n                            " +
                              _vm._s(link.icon) +
                              "\n                        "
                          )
                        ])
                      ],
                      1
                    )
              ],
              1
            )
          }),
          0
        )
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "header-brand-search" }, [
      _c("div", { staticClass: "al-container al-mobile-container" }, [
        _c(
          "div",
          { staticClass: "logo" },
          [
            _c("router-link", { attrs: { to: { name: "landingPage" } } }, [
              _c("img", { attrs: { src: "/images/logo.png", alt: "Logo" } })
            ])
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "mobile-menu" },
          [
            _c("v-icon", { attrs: { size: "32", color: "#700b0d" } }, [
              _vm._v("fas fa-bars")
            ])
          ],
          1
        ),
        _vm._v(" "),
        _c("div", { staticClass: "search" }, [
          _c("input", { ref: "address", attrs: { type: "text" } })
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/components/landingPage/Header.vue":
/*!************************************************************!*\
  !*** ./resources/js/src/components/landingPage/Header.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Header_vue_vue_type_template_id_40dd73b6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Header.vue?vue&type=template&id=40dd73b6& */ "./resources/js/src/components/landingPage/Header.vue?vue&type=template&id=40dd73b6&");
/* harmony import */ var _Header_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Header.vue?vue&type=script&lang=js& */ "./resources/js/src/components/landingPage/Header.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Header_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Header_vue_vue_type_template_id_40dd73b6___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Header_vue_vue_type_template_id_40dd73b6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/components/landingPage/Header.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/components/landingPage/Header.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/src/components/landingPage/Header.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_babel_loader_lib_index_js_ref_10_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/babel-loader/lib??ref--10-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Header.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/landingPage/Header.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_babel_loader_lib_index_js_ref_10_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/components/landingPage/Header.vue?vue&type=template&id=40dd73b6&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/src/components/landingPage/Header.vue?vue&type=template&id=40dd73b6& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_template_id_40dd73b6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Header.vue?vue&type=template&id=40dd73b6& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/landingPage/Header.vue?vue&type=template&id=40dd73b6&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_template_id_40dd73b6___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_template_id_40dd73b6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);