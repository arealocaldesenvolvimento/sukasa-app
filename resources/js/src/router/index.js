import Vue from "vue";
import VueRouter from "vue-router";

import Site from "../views/Site";
import Home from "../views/Site/home";
import Contato from "../views/Site/contato";
import Blog from "../views/Site/blog";
import Intro from "../views/Site/intro";
import Register from "../views/Site/register";

import Login from "../views/Auth/login";

import Admin from "../views/Admin";
import Dashboard from "../views/Admin/dashboard";
import Properties from "../views/Admin/properties";
import PropertiesList from "../views/Admin/properties/list";
import editProperty from "../views/Admin/properties/edit";
import createProperty from "../views/Admin/properties/create";

import News from "../views/Admin/noticias";
import NewsList from "../views/Admin/noticias/list";
import NewsCreate from "../views/Admin/noticias/create";
import NewsEdit from "../views/Admin/noticias/edit";

import Planos from "../views/Admin/planos";
import PlanosList from "../views/Admin/planos/list";
import PlanosCreate from "../views/Admin/planos/create";
import PlanosEdit from "../views/Admin/planos/edit";

import Users from "../views/Admin/users";
import UsersList from "../views/Admin/users/list";
import UsersCreate from "../views/Admin/users/create";
import UsersEdit from "../views/Admin/users/edit";

import Client from "../views/Client";
import AnunciosList from "../views/Client/list";
import AnunciosCreate from "../views/Client/create";
import AnunciosEdit from "../views/Client/edit";

import ClientPlanos from "../views/Client/planos";
import ClientPlanosList from "../views/Client/planos/list.vue";


Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        name: "landingPage",
        component: Intro
    },

    {
        path: "/home",
        component: Site,
        children: [
            {
                path: "",
                name: "home",
                component: Home
            }
        ]
    },

    {
        path: "/contato",
        component: Site,
        children: [
            {
                path: "",
                name: "contato",
                component: Contato
            }
        ]
    },

    {
        path: "/register",
        component: Site,
        children: [
            {
                path: "",
                name: "registrar",
                component: Register
            }
        ]
    },

    {
        path: "/blog",
        component: Site,
        children: [
            {
                path: "",
                name: "blog",
                component: Blog
            },
            {
                path: "post",
                name: "post",
                props: true,
                component: () => import("../views/Site/post")
            }
        ]
    },

    {
        path: "/login",
        name: "login",
        component: Login,
        meta: {
            auth: false
        }
    },

    {
        path: "/painel",
        component: Admin,
        meta: {
            auth: true
        },

        children: [
            {
                path: "",
                name: "dashboard",
                component: Dashboard
            },
            {
                path: "imoveis",
                component: Properties,
                children: [
                    {
                        path: "",
                        name: "imoveis",
                        component: PropertiesList
                    },
                    {
                        path: "novo",
                        name: "createImovel",
                        component: createProperty
                    },
                    {
                        path: "editar",
                        name: "editImovel",
                        component: editProperty
                    }
                ]
            },
            {
                path: "noticias",
                component: News,
                children: [
                    {
                        path: "",
                        name: "noticias",
                        component: NewsList
                    },
                    {
                        path: "novo",
                        name: "createPost",
                        component: NewsCreate
                    },
                    {
                        path: "editar",
                        name: "editPost",
                        component: NewsEdit
                    }
                ]
            },
            {
                path: "planos",
                component: Planos,
                children: [
                    {
                        path: "",
                        name: "planos",
                        component: PlanosList
                    },
                    {
                        path: "novo",
                        name: "createPlano",
                        component: PlanosCreate
                    },
                    {
                        path: "editar",
                        name: "editPlanos",
                        component: PlanosEdit
                    }
                ]
            },
            {
                path: "users",
                component: Users,
                children: [
                    {
                        path: "",
                        name: "users",
                        component: UsersList
                    },
                    {
                        path: "novo",
                        name: "createUser",
                        component: UsersCreate
                    },
                    {
                        path: "editar",
                        name: "editUser",
                        component: UsersEdit
                    }
                ]
            }
        ],
    },
    {
        path: "/anunciante",
        component: Client,
        meta: {
            auth: true
        },
        // meta: {
        //     auth: {
        //         role: 'client',
        //         redirect: '/anunciante'
        //     }
        // },
        children: [
            {
                path: "",
                name: "dashboardAnunciante",
                component: AnunciosList
            },
            {
                path: "novo",
                name: "createAnuncio",
                component: AnunciosCreate
            },
            {
                path: "novo",
                name: "editAnuncio",
                component: AnunciosEdit
            },

            {
                path: "planos",
                component: ClientPlanos,
                children: [
                    {
                        path: "",
                        name: "planosAnunciante",
                        component: ClientPlanosList
                    },
                ]
            },
        ],
    }
];

const router = new VueRouter({
    history: true,
    mode: "history",
    routes
});

// router.beforeEach(async (to, from, next) => {
//     await router.app.$nextTick();
//     console.log(from.path);
//     console.log(to.path);
//     next();
// });

Vue.router = router;

export default router;
