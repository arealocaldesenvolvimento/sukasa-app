import Vue from "vue";
import Vuex from "vuex";

import { PropertyModule, FiltersModule } from "../views/Site/store";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        PropertyModule,
        FiltersModule
    },
});
