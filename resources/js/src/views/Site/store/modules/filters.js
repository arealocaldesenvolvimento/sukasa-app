const getDefaultState = () => {
    return {
        filters: []
    };
};

const state = getDefaultState();

const actions = {};

const mutations = {
    MUTATE_FILTERS(state, payload) {
        console.log('payload', payload);
        const payloadKeys = Object.keys(payload);

        state.filters = state.filters.filter(filter => {
            const filterKeys = Object.keys(filter);

            return !filterKeys.some(key => payloadKeys.includes(key));
        });

        state.filters.push(payload);
    },

    DELETE_FILTERS(state) {
        Object.assign(state, getDefaultState());
    },

    REMOVE_FILTER(state, filterKey) {
        state.filters = state.filters.filter(filter => {
            return !Object.keys(filter).includes(filterKey);
        });
    }
};

export default {
    namespaced: true,
    state,
    getters: {
        filters: ({ filters }) => {
            return filters;
        },
        activeFilters: state => {
            return state.filters;
        }
    },
    actions,
    mutations
};
