import axios from "axios";

export default {
    namespaced: true,

    state: {
        properties: [],
        properties_holder: [],
        properties_holder_filter: [],
        locations: [],
        mapCenter: []
    },

    getters: {
        mapCenter: state => {
            return state.mapCenter;
        },

        locations: state => {
            return state.locations;
        },

        properties: state => {
            return state.properties;
        },

        properties_holder: state => {
            return state.properties_holder;
        },

        properties_holder_filter: ({ properties_holder_filter }) => {
            return properties_holder_filter;
        }
    },

    mutations: {
        MUTATE_PROPERTIES_FILTER_HOLDER(state, payload) {
            state.properties_holder_filter = payload;
        },

        MUTATE_PROPERTIES_HOLDER(state, { ...payload }) {
            state.properties_holder = payload;
        },

        MUTATE_PROPERTIES(state, payload) {
            const { properties, filters } = payload;

            console.log('properties', properties)
            console.log('filters', filters)

            if (filters && filters.length > 0) {
                state.properties = applyFilters(properties, filters);
            } else {
                state.properties = properties;
            }
        },

        MUTATE_LOCATIONS(state, { ...payload }) {
            for (const i in payload) {
                // Defina valores fixos para latitude e longitude (substitua pelos valores desejados)
                const latitude = 37.7749;
                const longitude = -122.4194;

                state.locations = [
                    ...state.locations,
                    {
                        item: payload[i],
                        position: {
                            lat: parseFloat(latitude),
                            lng: parseFloat(longitude)
                        }
                    }
                ];
            }
        },

        MUTATE_MAPCENTER(state, { location }) {
            state.mapCenter = location;
        }
    },

    actions: {
        async FETCH_PROPERTIES({ commit }) {
            const response = await axios.get("/properties");
            commit("MUTATE_PROPERTIES_HOLDER", response.data);
        },
        async FETCH_PROPERTIES_PUBLISHED({ commit }) {
            const response = await axios.get("/property/get_published");

            commit("MUTATE_PROPERTIES_HOLDER", response.data);
        },

        async FETCH_CURRENT_LOCATION({ commit }) {
            const response = await axios.post(
                `https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyDXBId8WK-gUwxQzdfF9Sb-XL_u0nn6-04`
            );
            commit("MUTATE_MAPCENTER", response.data);
        }
    }
};

function applyFilters(properties, filters) {
    let filteredProperties = properties;

    const convertAvaliationValue = (value) => {
        return parseFloat(value.replace(/\./g, '').replace(',', '.'));
    };

    filters.forEach(filterObject => {
        if ('minPrice' in filterObject || 'maxPrice' in filterObject) {
            filteredProperties = filteredProperties.filter(property => {
                const avaliationValue = convertAvaliationValue(property.avaliation_value);
                if ('minPrice' in filterObject && avaliationValue < filterObject.minPrice) {
                    return false;
                }
                if ('maxPrice' in filterObject && avaliationValue > filterObject.maxPrice) {
                    return false;
                }
                return true;
            });
        }
    });

    filters.forEach(filterObject => {
        for (const category in filterObject) {
            if (category !== 'maxPrice' && category !== 'minPrice') {
                const filterValues = filterObject[category];
                if (Array.isArray(filterValues)) {
                    let categoryFiltered = [];
                    filterValues.forEach(item => {
                        categoryFiltered.push(...filteredProperties.filter(
                            property => property[category] === item
                        ));
                    });
                    filteredProperties = categoryFiltered;
                } else {
                    console.error(`Filtro para categoria '${category}' não é um array.`);
                }
            }
        }
    });

    console.log("result", filteredProperties)

    return filteredProperties;
}




