<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Api\PropertyController;
use App\Http\Controllers\Api\ImportController;
use App\Http\Controllers\Api\PostController;
use App\Http\Controllers\Api\ContactController;
use App\Http\Controllers\Api\PostCategoryController;
use App\Http\Controllers\Api\UsersController;
use App\Http\Controllers\Api\PlanosController;
use App\Http\Controllers\Api\AnuncioController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/**
 * Auth routes
 */
Route::post('/auth/login', [AuthController::class, 'login']);
Route::post('/auth/register', [AuthController::class, 'register']);
Route::get('/auth/refresh', [AuthController::class, 'refresh']);

Route::group(['middleware' => 'auth:api'], function(){
    Route::get('auth/user', [AuthController::class, 'user']);
    Route::post('auth/logout', [AuthController::class, 'logout']);

});
Route::post('import', ImportController::class);

Route::post('/import-properties', [ImportController::class, 'importProperties']);
Route::post('/import-single-property', [ImportController::class, 'importSingleProperty']);
Route::post('/delete-all-properties', [ImportController::class, 'deleteAllProperties']);
Route::get('/test-location-fetch/{query}', [ImportController::class, 'testLocationFetch']);
/**
* Admin routes
*/
Route::apiResources([
    'properties' => PropertyController::class,
    'posts' => PostController::class,
    'users' => UsersController::class,
    'planos' => PlanosController::class,
]);
Route::get('painel/users/{id}/check_payment', [UsersController::class, 'checkPayment']);
Route::post('/property/get_published', [PropertyController::class, 'getPublished']);

// Rotas para buscar valores únicos
Route::get('/property/sale-modules', [PropertyController::class, 'getUniqueSaleModules']);
Route::get('/property/types', [PropertyController::class, 'getUniquePropertyTypes']);

/**
* Client routes
*/
Route::post('/contact', ContactController::class);
Route::get('filter/{option}', [PropertyController::class, 'filter']);
Route::get('/noticias/get_categories', [PostController::class, 'getCategories']);
Route::post('/property/delete_image', [PropertyController::class, 'deleteImage']);
Route::get('anunciante/anuncios', [AnuncioController::class, 'index']);
Route::get('anunciante/plano', [AnuncioController::class, 'getPlano']);
Route::get('anunciante/change_status/{id}', [AnuncioController::class, 'changeStatus']);
Route::delete('anunciante/anuncios/delete/{id}', [AnuncioController::class, 'destroy']);
Route::get('anunciante/contratar_plano/{id}', [PlanosController::class, 'buyPlano']);
Route::post('anunciante/store_plano', [PlanosController::class, 'storePlano']);
Route::get('anunciante/checar_pagamento', [PlanosController::class, 'checkPayment']);
