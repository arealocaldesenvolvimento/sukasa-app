const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.setPublicPath("public");
mix.webpackConfig({
    output: {
        chunkFilename: "js/chunks/[name].js"
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules(?!\/foundation-sites)|bower_components/,
                use: [
                    {
                        loader: "babel-loader",
                        options: Config.babel()
                    }
                ]
            }
        ]
    },
    resolve: {
        alias: {
            "@": path.resolve("resources/sass")
        }
    }
});
mix.js("resources/js/app.js", "public/js").version();
